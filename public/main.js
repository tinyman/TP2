function debounce(fn, time) {
    let timeout = null;
    return (...args) => {
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => fn.apply(null, args), time);
    };
}

function memoize2(fn) {
    const memoized = new Map();
    return async(first, second) => {
        const key = first + "|" + second;
        if (memoized.has(key)) return memoized.get(key);
        else {
            const res = await fn(first, second);
            memoized.set(key, res);
            return res;
        }
    };
}

const loadSuggestions = memoize2(function(terms, limit = 10) {
    const url = `/api?action=opensearch&format=json&formatversion=2&search=${terms}&namespace=0&limit=${limit}&suggest=true`;
    return new Promise((res, rej) => {
        const req = new XMLHttpRequest();
        req.onerror = () => rej(req.statusText);
        req.onload = () => {
            if (req.status === 200) res(JSON.parse(req.response));
            else rej(req.statusText);
        };
        req.open("GET", url);
        req.send();
    });
});

async function doSearch($textBox, $suggestions) {
    try {
        const terms = $textBox.value.trim();
        if (terms.length === 0) return;
        const result = await loadSuggestions(terms, 10);

        const range = document.createRange();
        range.selectNodeContents($suggestions);
        range.deleteContents();

        const labels = result[1];
        const links = result[3];
        const descriptions = result[2];
        labels.forEach((label, id) => {
            const link = document.createElement("a");
            link.href = links[id];
            link.innerText = label;
            link.tabIndex = id + 2;
            const descr = document.createElement("div");
            descr.innerText = descriptions[id];
            const suggestion = document.createElement("li");
            suggestion.appendChild(link);
            suggestion.appendChild(descr);

            $suggestions.appendChild(suggestion);
        });
    } catch (e) {
        console.error(e);
    }
}

function main() {
    const $form = document.querySelector("#searchform");
    const $suggestions = document.querySelector("#suggestions");
    const $search = document.querySelector("#search");
    $form.addEventListener("submit", ev => {
        ev.preventDefault();
        doSearch($search, $suggestions);
    });
    $search.addEventListener(
        "keydown",
        debounce(ev => ev.which !== 9 && doSearch($search, $suggestions), 150)
    );
}

window.onload = main;